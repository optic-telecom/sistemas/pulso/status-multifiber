import React, {useState, useEffect} from 'react';
import './App.css';
import {Table, Progress} from 'antd';
import 'antd/dist/antd.css';
import io from 'socket.io-client';

function App() {
  const [systemDataSource, setSystemDataSource] = useState([])
  const [tasksDataSource, setTasksDataSource] = useState([])
  const [message, setMessage] = useState('')

  const systemColumns = [
    {
      title: 'Sistema',
      dataIndex: 'system',
      key: 'system'
    },
    {
      title: 'Estado actual',
      dataIndex: 'status',
      key: 'status'
    }
  ]
  const tasksColumns = [
    {
      title: 'Tareas',
      dataIndex: 'tasks',
      key: 'tasks'
    },
    {
      title: 'Progreso actual',
      dataIndex: 'progress',
      key: 'progress'
    }
  ]

  const listenToSocket = () => {
    let last_msg='';
    const socket = io("https://status.multifiber.cl/");
    socket.on("message", function(msg) {
      //document.getElementById('message').innerHTML = msg;
      //console.log('Mensaje: ', msg)
      setMessage(msg);
    });
    socket.on("server-to-clients", function(message_raw) {
      last_msg = message_raw;
      const msg = JSON.parse(last_msg);
      
      /* if (msg.status_bar == 'status_pulso'){
        document.getElementById('status_pulso').style.width = msg.data + "%";
      }

      if (msg.status_bar == 'status_script_pulso'){
        document.getElementById('status_script_pulso').style.width = msg.data + "%";
      }*/

      //console.log('Mensaje server to client: ', message_raw, msg)
      if (msg.status_system) {
        setSystemDataSource(prevState=>{
            let isNewSystem = true;
            let actualSystems = prevState;
            const systemObj = {
                key: msg.status_system,
                system: msg.status_system,
                status: msg.status
            };
            actualSystems.forEach((systemObj, i)=>{
                if (systemObj.system === msg.status_system) {
                    isNewSystem = false
                    actualSystems = actualSystems.slice(0,i).concat(systemObj).concat(actualSystems.slice(i+1))
                }
            })
            if (isNewSystem) {
                actualSystems.push(systemObj)
            }
            return actualSystems;
        })
        
        /*if ( document.getElementById( msg.status_system )) {
          document.getElementById(msg.status_system).style.width = msg.data + "%";
        }else{
          var table = document.getElementById("tabla_sistemas");
          var row = table.insertRow(1);
          var cell1 = row.insertCell(0);
          var cell2 = row.insertCell(1);
          cell1.innerHTML = msg.status_system;
          cell2.innerHTML = msg.status;
          document.getElementById(msg.status_system).style.width = msg.data + "%";
        } */
      } 
      if (msg.status_bar) {
        setTasksDataSource(prevState=>{
            let isNewTask = true;
            let actualTasks = prevState;
            const taskObj = {
                key: msg.status_bar,
                tasks: msg.status_bar, 
                progress: <Progress percent={msg.data} status='active' />
            }
            actualTasks.forEach((task,i)=>{
                if (task.tasks === msg.status_bar) {
                    isNewTask = false;
                    actualTasks = actualTasks.slice(0,i).concat(taskObj).concat(actualTasks.slice(i+1))
                }
            })
            if (isNewTask) {
                actualTasks.push(taskObj)
            }
            return actualTasks;
        })

        /*if ( document.getElementById( msg.status_bar )) {
          document.getElementById(msg.status_bar).style.width = msg.data + "%";
        } else {
          var table = document.getElementById("tabla_scripts");
          var row = table.insertRow(1);
          var cell1 = row.insertCell(0);
          var cell2 = row.insertCell(1);
          cell1.innerHTML = msg.status_bar;
          cell2.innerHTML = '<div class="progress">'+
          '<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75"'+
          'aria-valuemin="0" aria-valuemax="100" style="width: 0%" id="'+msg.status_bar+'"></div></div>';

          document.getElementById(msg.status_bar).style.width = msg.data + "%";
        } */
      }   
    });      
    socket.emit('message-from-client-to-server', 'Hello World!');
  }

  useEffect(listenToSocket, [])
  
  return (
    <>
      <h1 className='center-text'>Status Multifiber</h1>
      <Table pagination={false} dataSource={systemDataSource} columns={systemColumns} />
      <Table pagination={false} dataSource={tasksDataSource} columns={tasksColumns} />
    </>
  );
}

export default App;
