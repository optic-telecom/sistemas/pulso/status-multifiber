/**
Jovan Pacheco 
13-08-2020 
**/
const express = require('express'); 
const app = express();
const server = app.listen(3000,
	console.log("Socket.io Servidor iniciado")
);
const io = require('socket.io')(server);

var redis = require('redis');  
var client1 = redis.createClient('redis://jovan:bfd3f0h6j1k7b4@localhost:6379/0');



const config = {
    port: 6379,
    host: "localhost",
    retries: 3,
    time_to_retry: 100,
    time_live: 3600 // tiempo de vida en segundos
}


app.use('/', express.static('build'));



client1.subscribe('server-to-clients');


client1.on('message', function(canal, msg) {  
   io.sockets.emit(canal, msg);
});




io.on('connection', (socket) => {
    console.log("Client connected!");
    socket.on('server-to-clients', (msg) => {
        console.log(msg);
        socket.broadcast.emit('server-to-clients',msg);
    })

});